import React, { useState } from 'react'
import { GoogleMap, useJsApiLoader, Marker, InfoWindow } from '@react-google-maps/api';


const containerStyle = {
    width: '600px',
    height: '600px'
};

const center = {
    name: "headquarter",
    lat: 40.730610,
    lng: -73.935242
};
const location1 = {
    name: "Biggest store",
    lat: 40.714847,
    lng: -73.898686
};

const contentString =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">BlahBlah</h1>' +
    '<div id="bodyContent">' +
    "<p>BlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlahBlah.</p>" +
    "</div>" +
    "</div>";


const markers = [

    {
        id: 1,
        name: "Headquarter",
        position: { lat: 40.714847, lng: -73.935242 }
    },
    {
        id: 2,
        name: "Biggest store",
        position: { lat: 40.740610, lng: -73.955242 }
    },
    {
        id: 3,
        name: "store 3",
        position: { lat: 40.730610, lng: -73.243683 }
    },
    {
        id: 4,
        name: "store 4",
        position: { lat: 40.712776, lng: -74.005974 }
    }
];

// function Map() {
//     const { isLoaded } = useJsApiLoader({
//         id: 'google-map-script',
//         googleMapsApiKey: "AIzaSyDkT1TTyAsntraTHdk5YjxEnhxVwAFZiOo"
//     })

// const [activeMarker, setActiveMarker] = useState(null);

// const handleActiveMarker = (marker) => {
//     if (marker === activeMarker) {
//         return;
//     }
//     setActiveMarker(marker);
// };

//     const handleOnLoad = (map) => {
//         // const bounds = new google.maps.LatLngBounds();
//         const bounds = new window.google.maps.LatLngBounds(center);
//         markers.forEach(({ position }) => bounds.extend(position));
//         map.fitBounds(bounds);
//     };

//     return (
//         <GoogleMap
//             onLoad={handleOnLoad}
//             onClick={() => setActiveMarker(null)}
//             mapContainerStyle={{ width: "100vw", height: "100vh" }}
//         >
//             <Marker position={center} />

// {markers.map(({ id, name, position }) => (
//     <Marker
//         key={id}
//         position={position}
//         onClick={() => handleActiveMarker(id)}
//     >
//         {activeMarker === id ? (
//             <InfoWindow onCloseClick={() => setActiveMarker(null)}>
//                 <div>{name}</div>
//             </InfoWindow>
//         ) : null}
//     </Marker>
// ))}

//         </GoogleMap>
//     );
// }




function Map() {
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyDkT1TTyAsntraTHdk5YjxEnhxVwAFZiOo"
    })

    const [map, setMap] = React.useState(null)

    const onLoad = React.useCallback(function callback(map) {
        // This is just an example of getting and using the map instance!!! don't just blindly copy!
        const bounds = new window.google.maps.LatLngBounds(center);
        map.fitBounds(bounds);

        setMap(map)
    }, [])

    const onUnmount = React.useCallback(function callback(map) {
        setMap(null)
    }, [])



    const infowindow = new InfoWindow({
        content: contentString,
        ariaLabel: "BlahBlah",
    });

    const [activeMarker, setActiveMarker] = useState(null);

    const handleActiveMarker = (marker) => {
        if (marker === activeMarker) {
            return;
        }
        setActiveMarker(marker);
    };

    return isLoaded ? (
        <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            zoom={8}
            onLoad={onLoad}
            onUnmount={onUnmount}
        >
            { /* Child components, such as markers, info windows, etc. */}
            {/* <Marker position={center} />
            <Marker position={location1} /> */}
            {markers.map(({ id, name, position }) => (
                <Marker
                    key={id}
                    position={position}
                    onClick={() => handleActiveMarker(id)}
                >
                    {activeMarker === id ? (
                        <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                            <div>{name}</div>
                        </InfoWindow>
                    ) : null}
                </Marker>
            ))}
        </GoogleMap>
    ) : <></>
}

// export default React.memo(Map)
export default Map
